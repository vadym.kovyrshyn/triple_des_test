﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main(string[] args)
        {
            string result = Decrypt3DES("769BE09249941F66EE0FB37C30B84150", "12345679821043649597402728752072");
        }

        public static string Decrypt3DES(string encryptedString, string key)
        {
            byte[] toEncryptArray = Enumerable.Range(0, encryptedString.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(encryptedString.Substring(x, 2), 16))
                     .ToArray();
            byte[] keyArray = Enumerable.Range(0, key.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(key.Substring(x, 2), 16))
                     .ToArray();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.None;
            tdes.KeySize = 192;
            tdes.Key = keyArray;
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}
